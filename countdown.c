///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
   int ret;
   struct tm info;
   struct tm realTime;
   char buffer[80];

   
   realTime.tm_year = 2021 - 1900;
   realTime.tm_mon = 2 - 1;
   realTime.tm_mday = 12;
   realTime.tm_hour = 6;
   realTime.tm_min = 20;
   realTime.tm_sec = 57;
   realTime.tm_isdst = -1;

   info.tm_year = 2014 - 1900;
   info.tm_mon = 1 - 1;
   info.tm_mday = 21;
   info.tm_hour = 16;
   info.tm_min = 26;
   info.tm_sec = 7;
   info.tm_isdst = -1;

   ret = mktime(&info);
   if( ret == -1 ) {
      printf("Error: unable to make time using mktime\n");
   } else {
      strftime(buffer, sizeof(buffer), "Reference Time: %a %b %I:%M:%S %p %Y\n", &info );
      printf(buffer);
   }
   int yearsPast = realTime.tm_year - info.tm_year;
   for(int x = 0; x<7; x++){
       printf("Years: %d    ", yearsPast);
       strftime(buffer, sizeof(buffer), "Days: %d   Hours:%I   Minutes:%M   Seconds:%S\n", &realTime);
       printf(buffer);
       realTime.tm_sec++;
       sleep(1);
       if(realTime.tm_sec == 60){
           realTime.tm_sec = 0;
           realTime.tm_min++;
       }
   }

   return(0);
}
